package com.roger.ffmpeg.listener

import com.roger.ffmpeg.model.LrcLine

interface OnLrcListener {

    fun onLrcSeek(position: Int, lrcLine: LrcLine)
}
