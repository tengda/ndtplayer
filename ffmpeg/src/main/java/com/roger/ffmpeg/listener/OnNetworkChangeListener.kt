package com.roger.ffmpeg.listener

interface OnNetworkChangeListener {

    fun onNetworkChange()

}