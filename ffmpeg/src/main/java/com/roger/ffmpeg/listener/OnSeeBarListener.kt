package com.roger.ffmpeg.listener

interface OnSeeBarListener {
    fun onProgress(index: Int, progress: Int)
}