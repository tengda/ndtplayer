package com.roger.live.magicfilter.utils;


import com.roger.live.magicfilter.advanced.MagicAmaroFilter;
import com.roger.live.magicfilter.advanced.MagicAntiqueFilter;
import com.roger.live.magicfilter.advanced.MagicBeautyFilter;
import com.roger.live.magicfilter.advanced.MagicBlackCatFilter;
import com.roger.live.magicfilter.advanced.MagicBrannanFilter;
import com.roger.live.magicfilter.advanced.MagicBrooklynFilter;
import com.roger.live.magicfilter.advanced.MagicCalmFilter;
import com.roger.live.magicfilter.advanced.MagicCoolFilter;
import com.roger.live.magicfilter.advanced.MagicCrayonFilter;
import com.roger.live.magicfilter.advanced.MagicEarlyBirdFilter;
import com.roger.live.magicfilter.advanced.MagicEmeraldFilter;
import com.roger.live.magicfilter.advanced.MagicEvergreenFilter;
import com.roger.live.magicfilter.advanced.MagicFreudFilter;
import com.roger.live.magicfilter.advanced.MagicHealthyFilter;
import com.roger.live.magicfilter.advanced.MagicHefeFilter;
import com.roger.live.magicfilter.advanced.MagicHudsonFilter;
import com.roger.live.magicfilter.advanced.MagicImageAdjustFilter;
import com.roger.live.magicfilter.advanced.MagicInkwellFilter;
import com.roger.live.magicfilter.advanced.MagicKevinFilter;
import com.roger.live.magicfilter.advanced.MagicLatteFilter;
import com.roger.live.magicfilter.advanced.MagicLomoFilter;
import com.roger.live.magicfilter.advanced.MagicN1977Filter;
import com.roger.live.magicfilter.advanced.MagicNashvilleFilter;
import com.roger.live.magicfilter.advanced.MagicNostalgiaFilter;
import com.roger.live.magicfilter.advanced.MagicPixelFilter;
import com.roger.live.magicfilter.advanced.MagicRiseFilter;
import com.roger.live.magicfilter.advanced.MagicRomanceFilter;
import com.roger.live.magicfilter.advanced.MagicSakuraFilter;
import com.roger.live.magicfilter.advanced.MagicSierraFilter;
import com.roger.live.magicfilter.advanced.MagicSketchFilter;
import com.roger.live.magicfilter.advanced.MagicSkinWhitenFilter;
import com.roger.live.magicfilter.advanced.MagicSunriseFilter;
import com.roger.live.magicfilter.advanced.MagicSunsetFilter;
import com.roger.live.magicfilter.advanced.MagicSutroFilter;
import com.roger.live.magicfilter.advanced.MagicSweetsFilter;
import com.roger.live.magicfilter.advanced.MagicTenderFilter;
import com.roger.live.magicfilter.advanced.MagicToasterFilter;
import com.roger.live.magicfilter.advanced.MagicValenciaFilter;
import com.roger.live.magicfilter.advanced.MagicWaldenFilter;
import com.roger.live.magicfilter.advanced.MagicWarmFilter;
import com.roger.live.magicfilter.advanced.MagicWhiteCatFilter;
import com.roger.live.magicfilter.advanced.MagicXproIIFilter;
import com.roger.live.magicfilter.base.MagicLookupFilter;
import com.roger.live.magicfilter.base.gpuimage.GPUImageBrightnessFilter;
import com.roger.live.magicfilter.base.gpuimage.GPUImageContrastFilter;
import com.roger.live.magicfilter.base.gpuimage.GPUImageExposureFilter;
import com.roger.live.magicfilter.base.gpuimage.GPUImageFilter;
import com.roger.live.magicfilter.base.gpuimage.GPUImageHueFilter;
import com.roger.live.magicfilter.base.gpuimage.GPUImageSaturationFilter;
import com.roger.live.magicfilter.base.gpuimage.GPUImageSharpenFilter;

public class MagicFilterFactory{

    public static GPUImageFilter initFilters(MagicFilterType type) {
        switch (type) {
            case NONE:
                return new GPUImageFilter();
            case WHITECAT:
                return new MagicWhiteCatFilter();
            case BLACKCAT:
                return new MagicBlackCatFilter();
            case SKINWHITEN:
                return new MagicSkinWhitenFilter();
            case BEAUTY:
                return new MagicBeautyFilter();
            case ROMANCE:
                return new MagicRomanceFilter();
            case SAKURA:
                return new MagicSakuraFilter();
            case AMARO:
                return new MagicAmaroFilter();
            case WALDEN:
                return new MagicWaldenFilter();
            case ANTIQUE:
                return new MagicAntiqueFilter();
            case CALM:
                return new MagicCalmFilter();
            case BRANNAN:
                return new MagicBrannanFilter();
            case BROOKLYN:
                return new MagicBrooklynFilter();
            case EARLYBIRD:
                return new MagicEarlyBirdFilter();
            case FREUD:
                return new MagicFreudFilter();
            case HEFE:
                return new MagicHefeFilter();
            case HUDSON:
                return new MagicHudsonFilter();
            case INKWELL:
                return new MagicInkwellFilter();
            case KEVIN:
                return new MagicKevinFilter();
            case LOCKUP:
                return new MagicLookupFilter("");
            case LOMO:
                return new MagicLomoFilter();
            case N1977:
                return new MagicN1977Filter();
            case NASHVILLE:
                return new MagicNashvilleFilter();
            case PIXAR:
                return new MagicPixelFilter();
            case RISE:
                return new MagicRiseFilter();
            case SIERRA:
                return new MagicSierraFilter();
            case SUTRO:
                return new MagicSutroFilter();
            case TOASTER2:
                return new MagicToasterFilter();
            case VALENCIA:
                return new MagicValenciaFilter();
            case XPROII:
                return new MagicXproIIFilter();
            case EVERGREEN:
                return new MagicEvergreenFilter();
            case HEALTHY:
                return new MagicHealthyFilter();
            case COOL:
                return new MagicCoolFilter();
            case EMERALD:
                return new MagicEmeraldFilter();
            case LATTE:
                return new MagicLatteFilter();
            case WARM:
                return new MagicWarmFilter();
            case TENDER:
                return new MagicTenderFilter();
            case SWEETS:
                return new MagicSweetsFilter();
            case NOSTALGIA:
                return new MagicNostalgiaFilter();
            case SUNRISE:
                return new MagicSunriseFilter();
            case SUNSET:
                return new MagicSunsetFilter();
            case CRAYON:
                return new MagicCrayonFilter();
            case SKETCH:
                return new MagicSketchFilter();
            //image adjust
            case BRIGHTNESS:
                return new GPUImageBrightnessFilter();
            case CONTRAST:
                return new GPUImageContrastFilter();
            case EXPOSURE:
                return new GPUImageExposureFilter();
            case HUE:
                return new GPUImageHueFilter();
            case SATURATION:
                return new GPUImageSaturationFilter();
            case SHARPEN:
                return new GPUImageSharpenFilter();
            case IMAGE_ADJUST:
                return new MagicImageAdjustFilter();
            default:
                return null;
        }
    }
}
