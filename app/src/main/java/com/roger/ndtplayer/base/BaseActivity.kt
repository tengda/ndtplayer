package com.roger.ndtplayer.base

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gyf.immersionbar.ImmersionBar
import com.roger.ndtplayer.commom.USER_LOGIN_STATE
import com.roger.ndtplayer.commom.core.MMKVHelper
import com.roger.ndtplayer.commom.dialog.ProgressDialogFragment
import com.tencent.mmkv.MMKV

abstract class BaseActivity : AppCompatActivity() {

    private lateinit var progressDialogFragment: ProgressDialogFragment
    val myMMKV: MMKV by lazy { MMKV.defaultMMKV() }

    abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        initStateBar()
        val bundle = intent.extras
        initParams(bundle)
        initView()
        initData()
    }
    /**
     * 数据初始化相关
     */
    open fun initView() {
        // Override if need
    }

    /**
     * 懒加载数据
     */
    open fun initData() {
        // Override if need
    }

    /**
     * [初始化参数]
     *
     * @param params
     */
    open fun initParams(params: Bundle?){}

    //初始化状态栏
    private fun initStateBar() {
        ImmersionBar.with(this)
            .autoDarkModeEnable(true)
            .transparentStatusBar() //                .statusBarColor(R.color.white)
            //                .fitsSystemWindows(true)  //使用该属性,必须指定状态栏颜色
            .statusBarDarkFont(true, 0.2f)
            .init()
    }

    //设置标题
    open fun setWhiteTitle() {
        ImmersionBar.with(this).reset()
            .autoDarkModeEnable(true)
            .statusBarDarkFont(false, 0.2f)
            .init()
    }

    //设置标题
    open fun setBlackTitle() {
        ImmersionBar.with(this).reset()
            .autoDarkModeEnable(true)
            .statusBarDarkFont(true, 0.2f)
            .init()
    }

    /**
     * 是否登录，如果登录了就执行then，没有登录就直接跳转登录界面
     * @return true-已登录，false-未登录
     */
    fun checkLogin(then: (() -> Unit)? = null): Boolean {
        return if (MMKVHelper.getMMKVValue(USER_LOGIN_STATE,false)) {
            then?.invoke()
            true
        } else {
//            ActivityHelper.startActivity(LoginActivity::class.java)
            false
        }
    }

    /**
     * 显示加载(转圈)对话框
     */
    fun showProgressDialog( message: String) {
        if (!this::progressDialogFragment.isInitialized) {
            progressDialogFragment = ProgressDialogFragment.newInstance()
        }
        if (!progressDialogFragment.isAdded) {
            progressDialogFragment.showDialog(supportFragmentManager, message)
        }
    }

    /**
     * 隐藏加载(转圈)对话框
     */
    fun dismissProgressDialog() {
        if (this::progressDialogFragment.isInitialized && progressDialogFragment.isVisible) {
            progressDialogFragment.dismissAllowingStateLoss()
        }
    }

}
