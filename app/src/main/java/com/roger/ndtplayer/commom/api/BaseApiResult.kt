package com.roger.ndtplayer.commom.api

import androidx.annotation.Keep

@Keep
data class BaseApiResult<T>(var success: ApiResult<T>) {
}