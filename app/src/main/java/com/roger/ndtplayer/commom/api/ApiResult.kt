package com.roger.ndtplayer.commom.api

import androidx.annotation.Keep

/**
 * ApiResult
 */
@Keep
data class ApiResult<T>(
    var code: String,
    var message: String,
    private val data: T?,
) {
    fun apiData(): T {
//        if (code == "00000" && data != null) {
        if (code == "00000") {
            if(data == null) {
                return "success" as T
            }
            return data
        } else {
            if (message.isNullOrEmpty()) {
                throw ApiException(code, message)
            } else {
                throw ApiException(code, message)
            }
        }
    }
}
