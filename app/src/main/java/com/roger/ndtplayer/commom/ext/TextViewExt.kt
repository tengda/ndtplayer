package com.roger.ndtplayer.commom.ext

import android.text.InputFilter
import android.widget.EditText
import android.widget.TextView

/**
 *防止textview输入为空报错
 */
inline fun TextView.setTextFilter(str: String) {
    text = if (str.isNullOrEmpty() || "null" == str) {
        ""
    } else {
        str
    }
}

/**
 *防止textview输入为空报错 并填入默认值
 */
inline fun TextView.setTextNullDefault(str: String, default: String) {
    if (str.isNullOrEmpty() || "null" == str) {
        text = default
    } else {
        text = str
    }
}

/**
 * 设置银行卡模式
 */
inline fun TextView.formatBankCardNum(str: String) {
    if (str.isNullOrEmpty() || "null" == str) {
        text = ""
    } else {
        val builder = StringBuilder()
        for (i in str.indices) {
            builder.append(str[i])
            if (i == 3 || i == 7 || i == 11 || i == 15) {
                if (i != str.length - 1) builder.append(" ")
            }
        }
        text = builder.toString()
    }
}

/**
 * 设置输入框价格模式 两位小数点
 */
inline fun EditText.setPriceEdit() {

    filters = arrayOf(InputFilter { source, start, end, dest, dstart, dend -> // source:当前输入的字符
        // start:输入字符的开始位置
        // end:输入字符的结束位置
        // dest：当前已显示的内容
        // dstart:当前光标开始位置
        // dent:当前光标结束位置
        //Log.e("", "source=" + source + ",start=" + start + ",end=" + end + ",dest=" + dest.toString() + ",dstart=" + dstart + ",dend=" + dend);
        if (dest.isEmpty() && source == ".") {
            return@InputFilter "0."
        }
        val dValue = dest.toString()
        val splitArray = dValue.split("\\.".toRegex()).toTypedArray()
        if (splitArray.size > 1) {
            val dotValue = splitArray[1]
            //                if (dotValue.length() == 2) {//输入框小数的位数是2的情况，整个输入框都不允许输入
//                    return "";
//                }
            if (dotValue.length == 2 && dest.length - dstart < 3) { //输入框小数的位数是2的情况时小数位不可以输入，整数位可以正常输入
                return@InputFilter ""
            }
        }
        null
    })
}