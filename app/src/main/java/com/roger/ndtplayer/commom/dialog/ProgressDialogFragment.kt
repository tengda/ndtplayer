package com.roger.ndtplayer.commom.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.roger.ndtplayer.R
import com.roger.ndtplayer.databinding.FragmentProgressDialogBinding

/**
 * ProgressDialogFragment
 * 进度框
 */
class ProgressDialogFragment : DialogFragment() {

    private var messageRes: String? = null
    lateinit var mDataBind: FragmentProgressDialogBinding

    companion object {
        fun newInstance() = ProgressDialogFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mDataBind = DataBindingUtil.inflate<FragmentProgressDialogBinding>(
            inflater,
            R.layout.fragment_progress_dialog,
            container,
            false
        )
        mDataBind.lifecycleOwner = this
        mDataBind.executePendingBindings()
        return mDataBind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDataBind.msgTv.text = messageRes ?: "正在加载中"
    }

    fun showDialog(
        fragmentManager: FragmentManager,
        messageRes: String,
        isCancelable: Boolean = true
    ) {
        this.messageRes = messageRes
        this.isCancelable = isCancelable
        try {
            show(fragmentManager, "progressDialogFragment")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}