package com.roger.ndtplayer.commom.api

import okhttp3.RequestBody
import retrofit2.http.*

/**
 * Created by admin
 */

const val PROJECTNAME = "jiujiu"


interface ApiService {

    companion object {
        const val BASE_URL = "https://xiaomeis-apigw-test.99elink.com/"
//        const val PDF_URL =
//            "http://99elink-static-file.oss-cn-beijing.aliyuncs.com/Asset/service_agreement.pdf"


        const val loginAction = "Common.Gateway.Permission.userLogin"
        const val getWaybill = "Common.Transport.ShipperCarrier.waybillList"
        const val workBenchPartStatistics =
            "Common.Transport.ShipperCarrier.workBenchPartStatistics"
        const val metodoMain = "Common.Transport.ShipperCarrier.index"
        const val getUserInfo = "Common.User.User.viewUserInfo"
    }
    @FormUrlEncoded
    @POST("v1/user/user/resetPasswordPerson")
    suspend fun modifyPassword(
        @Field("account_id") account_id: Int,
        @Field("old_password") old_password: String,
        @Field("first_password") first_password: String,
        @Field("confirm_password") confirm_password: String
    ): BaseApiResult<*>

    /**
     * type == 1 账号密码注册 type == 2 验证码注册
     */
    @FormUrlEncoded
    @POST("v1/user/user/userRegister")
    suspend fun registerAction(
        @Field("mobile_phone") mobile_phone: String,
        @Field("password") password: String,
        @Field("confirm_password") confirm_password: String,
        @Field("sms_code") sms_code: String,
        @Field("type") type: Int
    ): BaseApiResult<*>


    /**
     * 用户找回密码
     */
    @FormUrlEncoded
    @POST("v1/user/user/resetPassword")
    suspend fun forgetPasswordAction(
        @Field("mobile_phone") mobile_phone: String,
        @Field("new_password") new_password: String,
        @Field("confirm_password") confirm_password: String,
        @Field("sms_code") sms_code: String
    ): BaseApiResult<*>

    /**
     * 用户设置密码
     */
    @FormUrlEncoded
    @POST("v1/user/user/userUpdate")
    suspend fun setPasswordAction(
        @Field("password") password: String,
        @Field("account_id") account_id: Int
    ): BaseApiResult<*>


    /**
     * 个人身份证验证
     */
    @FormUrlEncoded
    @POST("v1/user/user/userCardAuthentication")
    suspend fun getUserCardAuthentication(
        @Field("account_id") account_id: Int,
        @Field("id_card") id_card: String,
        @Field("id_card_first") id_card_first: String,
        @Field("id_card_back") id_card_back: String,
        @Field("first_name") first_name: String,
        @Field("available_time") available_time: String,
    ): BaseApiResult<*>

    /**
     * 用户个人信息编辑
     */
    @FormUrlEncoded
    @POST("v1/user/user/userUpdate")
    suspend fun getUserUpdateInfo(
        @Field("account_id") account_id: Int,
        @Field("family_name") company_id: String,
        @Field("sex") sex: Int,
        @Field("avatar") avatar: String,
        @Field("mail") mail: String

    ): BaseApiResult<*>


    /**
     * 用户个人信息编辑
     */
//    @FormUrlEncoded
    @POST("v1/user/user/updateAvatar")
    suspend fun getUserUpdateInfo(
        @QueryMap paramsMap: HashMap<String, Any>
    ): BaseApiResult<*>
}