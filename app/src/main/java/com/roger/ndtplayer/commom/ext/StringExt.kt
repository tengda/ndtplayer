package com.roger.ndtplayer.commom.ext

import java.text.DecimalFormat


/**
 * 格式化两位小数
 */
inline fun String.format2Price(): String {
    if (isEmpty()) {
        return "0.00"
    }
    if (!contains(".")) {
        return this + ".00"
    }
    if (length - lastIndexOf(".") == 2) {
        return this + "0"
    }
    return this
}

/**
 * @param text
 * @return String    返回类型
 * @Title: fmtMicrometer
 * @Description: 格式化数字为千分位
 */

inline fun String.fmtMicrometer(): String {
    var df: DecimalFormat? = null
    df = if (indexOf(".") > 0) {
        if (length - indexOf(".") - 1 == 0) {
            DecimalFormat("###,##0.")
        } else if (length - indexOf(".") - 1 == 1) {
            DecimalFormat("###,##0.0")
        } else {
            DecimalFormat("###,##0.00")
        }
    } else {
        DecimalFormat("###,##0")
    }
    var number = 0.0
    number = try {
        toDouble()
    } catch (e: Exception) {
        0.0
    }
    return df.format(number)
}
