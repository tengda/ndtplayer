package com.roger.ndtplayer.commom.api

class ApiException(var code: String, override var message: String) : RuntimeException()