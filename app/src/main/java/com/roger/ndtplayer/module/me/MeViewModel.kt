package com.roger.ndtplayer.module.me

import android.os.Parcelable
import com.roger.ndtplayer.base.BaseViewModel
import kotlinx.android.parcel.Parcelize

@Parcelize
class MeViewModel : BaseViewModel(), Parcelable {

    val msgNumber = MutableIntLiveData()

}