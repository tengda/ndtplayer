package com.roger.ndtplayer.module.movie

import com.roger.ndtplayer.R
import com.roger.ndtplayer.base.BaseViewModel

class MovieViewModel : BaseViewModel() {

    val requestSucceesConfig = MutableBooleanLiveData()

    val requestSuccessOss = MutableBooleanLiveData(false)//请求成功

    val imgId = MutableStringLiveData("")//图片id
    val goodsType = MutableStringLiveData("")//货品品类
    val address = MutableStringLiveData("")//产地
    val number = MutableStringLiveData("")
    val meiType = MutableStringLiveData("")
    val reZhi = MutableStringLiveData("")
    val liuFen = MutableStringLiveData("")
    val quanShui = MutableStringLiveData("")
    val neiShui = MutableStringLiveData("")
    val huifafen = MutableStringLiveData("")
    val huifen = MutableStringLiveData("")

    val rezhiTips = MutableStringLiveData("0")//热值提示
    val liufenTips = MutableStringLiveData("0")//硫分提示

    val resository by lazy { MovieRepository() }




    fun getRandomTheme(): Int {
        return arrayListOf(
            R.style.FilePickerThemeRail,
            R.style.FilePickerThemeCrane,
            R.style.FilePickerThemeReply,
            R.style.FilePickerThemeShrine
        ).run {
            shuffle()
            first()
        }
    }
}