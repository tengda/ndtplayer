package com.roger.ndtplayer.module.player

import com.roger.ndtplayer.R
import com.roger.ndtplayer.base.BaseViewModel
import com.roger.ndtplayer.commom.USER_LOGIN_STATE
import com.roger.ndtplayer.commom.core.MMKVHelper

class MoviePlayerViewModel : BaseViewModel() {

    val loginResult = MutableBooleanLiveData(false)

    fun direct(){
        launchOnIO(
            block = {
                loginResult.value= MMKVHelper.getMMKVValue(USER_LOGIN_STATE,true)
            }
        )
    }

    fun getRandomTheme(): Int {
        return arrayListOf(
            R.style.FilePickerThemeRail,
            R.style.FilePickerThemeCrane,
            R.style.FilePickerThemeReply,
            R.style.FilePickerThemeShrine
        ).run {
            shuffle()
            first()
        }
    }
}