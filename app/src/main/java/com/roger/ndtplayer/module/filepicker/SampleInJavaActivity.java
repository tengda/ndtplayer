package com.roger.ndtplayer.module.filepicker;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import com.roger.filepicker.adapter.FileListAdapter;
import com.roger.filepicker.config.FileItemOnClickListener;
import com.roger.filepicker.config.FilePickerManager;
import com.roger.ndtplayer.R;

public class SampleInJavaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_activity_main);
        FilePickerManager.INSTANCE
                .from(this)
                .setItemClickListener(new FileItemOnClickListener() {
                    @Override
                    public void onItemClick(@NotNull FileListAdapter itemAdapter, @NotNull View itemView, int position) {
                        if (itemAdapter.getDataList() != null) {
                            Toast.makeText(SampleInJavaActivity.this, itemAdapter.getDataList().get(position).getFileName(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onItemChildClick(@NotNull FileListAdapter itemAdapter, @NotNull View itemView, int position) {
                        if (itemAdapter.getDataList() != null) {
                            Toast.makeText(SampleInJavaActivity.this, itemAdapter.getDataList().get(position).getFileName(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onItemLongClick(@NotNull FileListAdapter itemAdapter, @NotNull View itemView, int position) {
                        if (itemAdapter.getDataList() != null) {
                            Toast.makeText(SampleInJavaActivity.this, itemAdapter.getDataList().get(position).getFileName(), Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .enableSingleChoice()
                .forResult(101);
//        FilePickerManager.INSTANCE
    }
}
