package com.roger.ndtplayer.module

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.roger.ndtplayer.R
import com.roger.ndtplayer.base.BaseDBActivity
import com.roger.ndtplayer.commom.ext.singleClick
import com.roger.ndtplayer.databinding.ActivityMainBinding
import com.roger.ndtplayer.module.me.FragmentMe
import com.roger.ndtplayer.module.movie.FragmentMovie
import net.lucode.hackware.magicindicator.FragmentContainerHelper
import net.lucode.hackware.magicindicator.buildins.UIUtil
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView
import java.util.ArrayList

class MainActivity(override val layoutId: Int = R.layout.activity_main) :
    BaseDBActivity<MainViewModel, ActivityMainBinding>() {

    val channels = arrayOf("看看", "我的")
    val picList = intArrayOf(
        R.mipmap.movie_icon,
        R.mipmap.me_icon
    )

    override fun viewModelClass() = MainViewModel::class.java

    override fun initData() {
        super.initData()
    }

    override fun initParams(params: Bundle?) {
        super.initParams(params)
    }

    override fun initView() {
        super.initView()

        initMagicIndicator()
        mFragments.add(movieFragment)
        mFragments.add(meFragment)
        switchPages(0)
    }

    //    选择fragment
    private fun switchPages(index: Int) {
        val currentFragment = supportFragmentManager.fragments.find {
            it.isVisible && it in mFragments
        }
        val targetFragment = mFragments[index]
        supportFragmentManager.beginTransaction().apply {
            currentFragment?.let { if (it.isVisible) hide(it) }
            targetFragment?.let {
                if (it.isAdded) show(it) else add(R.id.fragment_container, it)
            }
        }.commit()
    }

    var position = 0
    lateinit var commonNavigatorAdapter: CommonNavigatorAdapter
    val mFragments: ArrayList<Fragment> = ArrayList()
    val movieFragment: FragmentMovie by lazy { FragmentMovie() }
    val meFragment: FragmentMe by lazy { FragmentMe() }
    val mFragmentContainerHelper: FragmentContainerHelper by lazy { FragmentContainerHelper() }


    //    初始化底部控件
    private fun initMagicIndicator() {
        var commonNavigator = CommonNavigator(this)
        commonNavigator.isAdjustMode = true
        commonNavigatorAdapter = object : CommonNavigatorAdapter() {
            override fun getCount(): Int {
                return channels.size
            }

            @RequiresApi(Build.VERSION_CODES.M)
            override fun getTitleView(context: Context, index: Int): IPagerTitleView {
                val commonPagerTitleView = CommonPagerTitleView(context)
                // load custom layout
                val customLayout: View =
                    LayoutInflater.from(context).inflate(R.layout.bottom_nav_item_layout, null)
                val titleImg = customLayout.findViewById<View>(R.id.title_img) as ImageView
                val titleText = customLayout.findViewById<View>(R.id.title_text) as TextView
                titleImg.setImageResource(picList[index])
                titleText.text = channels[index]
                commonPagerTitleView.setContentView(customLayout)
                commonPagerTitleView.onPagerTitleChangeListener =
                    object : CommonPagerTitleView.OnPagerTitleChangeListener {
                        override fun onSelected(index: Int, totalCount: Int) {
                            titleText.setTextColor(resources.getColor(R.color.bottom_text_select))
                            titleImg.isSelected = true
                        }

                        override fun onDeselected(index: Int, totalCount: Int) {
                            titleText.setTextColor(resources.getColor(R.color.bottom_text_select))
                            titleImg.isSelected = false
                        }

                        override fun onLeave(
                            index: Int,
                            totalCount: Int,
                            leavePercent: Float,
                            leftToRight: Boolean
                        ) {
                            titleText.setTextColor(resources.getColor(R.color.bottom_text_select))
                            titleImg.isSelected = false
                        }

                        override fun onEnter(
                            index: Int,
                            totalCount: Int,
                            enterPercent: Float,
                            leftToRight: Boolean
                        ) {
                            titleText.setTextColor(resources.getColor(R.color.bottom_text_select))
                            titleImg.isSelected = true
                        }
                    }
                commonPagerTitleView.singleClick {
                    position = index
                    getStatus()
                }
                return commonPagerTitleView
            }

            override fun getIndicator(context: Context): IPagerIndicator {
                val indicator = LinePagerIndicator(context)
                //                float navigatorHeight = context.getResources().getDimension(43);
                val navigatorHeight = UIUtil.dip2px(context, 56.0).toFloat()
                val borderWidth = UIUtil.dip2px(context, 1.0).toFloat()
                val lineHeight = navigatorHeight - 2 * borderWidth
                indicator.lineHeight = lineHeight
                //                indicator.setRoundRadius(lineHeight / 2);
                indicator.yOffset = borderWidth
                indicator.setColors(resources.getColor(R.color.white))
                return indicator
            }
        }
        commonNavigator.adapter = commonNavigatorAdapter
        mDataBind.magicIndicator.navigator = commonNavigator
        mFragmentContainerHelper.attachMagicIndicator(mDataBind.magicIndicator)
    }

    fun getStatus() {
        switchPages(position)
        mFragmentContainerHelper.handlePageSelected(position)
    }
}