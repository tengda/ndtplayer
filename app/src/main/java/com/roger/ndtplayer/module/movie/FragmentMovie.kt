package com.roger.ndtplayer.module.movie

import android.Manifest
import android.content.Context
import android.content.Intent
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.luck.picture.lib.PictureSelector
import com.luck.picture.lib.config.PictureConfig
import com.luck.picture.lib.config.PictureMimeType
import com.luck.picture.lib.entity.LocalMedia
import com.roger.filepicker.config.FilePickerManager
import com.roger.filepicker.engine.ImageEngine
import com.roger.ndtplayer.R
import com.roger.ndtplayer.base.BaseDBFragment
import com.roger.ndtplayer.commom.core.ActivityHelper
import com.roger.ndtplayer.commom.ext.singleClick
import com.roger.ndtplayer.databinding.FragMovieBinding
import com.roger.ndtplayer.module.filepicker.SampleActivity
import com.roger.ndtplayer.module.filepicker.SampleInJavaActivity
import com.roger.ndtplayer.module.nfc.NFCActivity
import com.roger.ndtplayer.module.player.MoviePlayerActivity
import com.roger.ndtplayer.util.GlideEngine
import com.tbruyelle.rxpermissions2.RxPermissions

/**
 * 订单发布
 */
class FragmentMovie(override val layoutId: Int = R.layout.frag_movie) :
    BaseDBFragment<MovieViewModel, FragMovieBinding>() {

    override fun viewModelClass() = MovieViewModel::class.java

    override fun initView() {
        super.initView()
        mDataBind.vm = mViewModel

        mDataBind.run {

            tvPlayerLocal.singleClick {
                ActivityHelper.startActivity(MoviePlayerActivity::class.java)
            }

            tvNfc.singleClick {
                ActivityHelper.startActivity(NFCActivity::class.java)
            }
        }
    }


    override fun observe() {
        super.observe()

        mViewModel.run {

        }
    }


    /**
     * 选择图片
     */
    fun choosePic(picList: List<LocalMedia>, code: Int) {
        PictureSelector.create(this)
            .openGallery(PictureMimeType.ofImage()) // 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
            .imageEngine(GlideEngine.createGlideEngine()) // 外部传入图片加载引擎，必传项
            .selectionMode(PictureConfig.SINGLE) // 多选 or 单选
            .previewImage(true) // 是否可预览图片
            .enableCrop(true) // 是否裁剪
            .compress(true) // 是否压缩
            .freeStyleCropEnabled(true) // 裁剪框是否可拖拽
            .showCropGrid(true) // 是否显示裁剪矩形网格 圆形裁剪时建议设为false
            .selectionMedia(picList) // 是否传入已选图片
            .forResult(code) //结果回调onActivityResult code
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != AppCompatActivity.RESULT_OK) {
            return
        }
    }


}