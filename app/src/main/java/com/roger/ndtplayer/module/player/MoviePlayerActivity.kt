package com.roger.ndtplayer.module.player

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.roger.filepicker.config.FilePickerManager
import com.roger.filepicker.engine.ImageEngine
import com.roger.ndtplayer.R
import com.roger.ndtplayer.base.BaseDBActivity
import com.roger.ndtplayer.databinding.ActivityMoviePlayerBinding
import com.tbruyelle.rxpermissions2.RxPermissions
import xyz.doikki.videocontroller.StandardVideoController
import java.util.*

/**
 * 引导页
 */
class MoviePlayerActivity(
    override val layoutId: Int = R.layout.activity_movie_player
) : BaseDBActivity<MoviePlayerViewModel, ActivityMoviePlayerBinding>() {

    override fun viewModelClass() = MoviePlayerViewModel::class.java
    override fun initParams(params: Bundle?) {
    }

    override fun initView() {
        super.initView()
        mDataBind.vm = mViewModel

//

        findViewById<TextView>(R.id.select_video).setOnClickListener {
            RxPermissions(this@MoviePlayerActivity)
                .request(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
                .subscribe { granted ->
                    if (granted) {
                        FilePickerManager
                            .from(this@MoviePlayerActivity)
                            .setTheme(mViewModel.getRandomTheme())
                            .imageEngine(object : ImageEngine {
                                override fun loadImage(
                                    context: Context?,
                                    imageView: ImageView?,
                                    url: String?,
                                    placeholder: Int
                                ) {
                                    // 应该使用 loadImage 传递过来的 context，而不是您自己的 context
                                    // You should use the context passed by loadImage(), not your own context
//                                    Glide
//                                        .with(context!!)
//                                        .load(url)
//                                        .into(imageView!!)

                                    mDataBind.player.setUrl(url) //设置视频地址

                                    val controller =
                                        StandardVideoController(this@MoviePlayerActivity)
                                    controller.addDefaultControlComponent("标题", false)
                                    mDataBind.player.setVideoController(controller) //设置控制器

                                    mDataBind.player.start() //开始播放，不调用则不自动播放
                                }
                            })
                            .enableSingleChoice()
                            .forResult(FilePickerManager.REQUEST_CODE)
                    }
                }
        }
    }

    override fun onPause() {
        super.onPause()
        mDataBind.player.pause()
    }

    override fun onResume() {
        super.onResume()
        mDataBind.player.resume()
    }

    override fun onDestroy() {
        super.onDestroy()
        mDataBind.player.release()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (!mDataBind.player.onBackPressed()) {
            super.onBackPressed()
        }
    }

}