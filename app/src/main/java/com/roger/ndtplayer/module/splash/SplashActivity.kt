package com.roger.ndtplayer.module.splash

import android.os.Bundle
import com.roger.ndtplayer.R
import com.roger.ndtplayer.base.BaseVmActivity
import com.roger.ndtplayer.commom.core.ActivityHelper
import com.roger.ndtplayer.module.MainActivity
import java.util.*

/**
 * 引导页
 */
class SplashActivity(
    override val layoutId: Int = R.layout.activity_splash
) : BaseVmActivity<SplashViewModel>() {

    val timer = Timer()
    override fun viewModelClass() = SplashViewModel::class.java
    override fun initParams(parms: Bundle?) {
    }

    var task: TimerTask = object : TimerTask() {
        override fun run() {
//            if (MMKVHelper.getMMKVValue(USER_LOGIN_STATE, true)) {
                ActivityHelper.startActivity(MainActivity::class.java)
//            } else {
//                ActivityHelper.startActivity(LoginActivity::class.java)
//            }
            ActivityHelper.finish(SplashActivity::class.java)
        }
    }

    override fun initView() {
        super.initView()
        timer.schedule(task, 1000)
        //是否需要弹出温馨提示Pop
//        var isShow = MMKVHelper.getMMKVValue(showWarmTips, false)
//        if (isShow) {
//            timer.schedule(task, 1000)
//        } else {
//            warmTipsPopup?.let { it.showPopupWindow() }
//        }
    }

}