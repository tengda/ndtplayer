package com.roger.ndtplayer.module.splash

import com.roger.ndtplayer.base.BaseViewModel
import com.roger.ndtplayer.commom.USER_LOGIN_STATE
import com.roger.ndtplayer.commom.core.MMKVHelper

class SplashViewModel : BaseViewModel() {

    val loginResult = MutableBooleanLiveData(false)

    fun direct(){
        launchOnIO(
            block = {
                loginResult.value= MMKVHelper.getMMKVValue(USER_LOGIN_STATE,true)
            }
        )
    }
}