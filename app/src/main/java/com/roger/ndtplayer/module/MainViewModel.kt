package com.roger.ndtplayer.module

import com.roger.ndtplayer.base.BaseViewModel

class MainViewModel : BaseViewModel() {

    private val repository by lazy { MainRepository() }

    val userStatus = MutableIntLiveData(0)//用户 审核状态0 未审核 1审核通过 2 审核中 3审核驳回
    val companyStatus = MutableIntLiveData(0)//公司 审核状态0 未审核 1审核通过 2 审核中 3审核驳回
    val companyApplyStateus = MutableIntLiveData(0) //	1 待开户 2 开户中 3开户成功 4 开户失败
    val changeStatus = MutableBooleanLiveData(false)
    val requestSucceed = MutableBooleanLiveData(false)
}