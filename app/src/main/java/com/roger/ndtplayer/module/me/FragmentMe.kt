package com.roger.ndtplayer.module.me

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.gyf.immersionbar.ImmersionBar
import com.luck.picture.lib.PictureSelector
import com.luck.picture.lib.config.PictureConfig
import com.luck.picture.lib.config.PictureMimeType
import com.luck.picture.lib.entity.LocalMedia
import com.roger.ndtplayer.R
import com.roger.ndtplayer.base.BaseDBFragment
import com.roger.ndtplayer.databinding.FragMeBinding
import com.roger.ndtplayer.util.GlideEngine

/**
 * 我的
 */
class FragmentMe(override val layoutId: Int = R.layout.frag_me) :
    BaseDBFragment<MeViewModel, FragMeBinding>() {

    override fun viewModelClass() = MeViewModel::class.java
    private lateinit var immBar: ImmersionBar


    override fun initView() {
        super.initView()
        immBar = ImmersionBar.with(this)

        mDataBind.run {
            vm = mViewModel
        }

    }

    /**
     * 选择图片
     */
    fun choosPic(picList: List<LocalMedia>, code: Int) {
        PictureSelector.create(this)
            .openGallery(PictureMimeType.ofImage()) // 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
            .imageEngine(GlideEngine.createGlideEngine()) // 外部传入图片加载引擎，必传项
            .selectionMode(PictureConfig.SINGLE) // 多选 or 单选
            .previewImage(true) // 是否可预览图片
            .enableCrop(true) // 是否裁剪
            .compress(true) // 是否压缩
            .freeStyleCropEnabled(true) // 裁剪框是否可拖拽
            .showCropGrid(true) // 是否显示裁剪矩形网格 圆形裁剪时建议设为false
            .selectionMedia(picList) // 是否传入已选图片
            .forResult(code) //结果回调onActivityResult code
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != AppCompatActivity.RESULT_OK) {
            return
        }
        when (requestCode) {
            22 -> {
                // 图片选择结果回调
                val headerPhotoList: List<LocalMedia> = PictureSelector.obtainMultipleResult(data)
                if (headerPhotoList.isEmpty()) {
                    return
                }
                val loadPath = headerPhotoList[0]?.compressPath.toString()
            }
        }
    }


    override fun observe() {
        super.observe()
        mViewModel.run {

        }
    }
}