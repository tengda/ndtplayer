package com.roger.ndtplayer.module.nfc

import com.roger.ndtplayer.R
import com.roger.ndtplayer.base.BaseDBActivity
import com.roger.ndtplayer.databinding.ActivityNfcBinding
import com.roger.ndtplayer.util.NfcUtils
import android.content.Intent
import com.roger.ndtplayer.util.LogUtils

/**
 * NFC page
 */
class NFCActivity(override val layoutId: Int = R.layout.activity_nfc) :
    BaseDBActivity<NFCViewModel, ActivityNfcBinding>() {

    override fun viewModelClass() = NFCViewModel::class.java

    override fun initView() {
        super.initView()

    }

    override fun initData() {
        super.initData()
        //nfc初始化设置
        //nfc初始化设置
        val nfcUtils = NfcUtils(this)
    }

    override fun observe() {
        super.observe()

    }

    override fun onResume() {
        super.onResume()
        //开启前台调度系统
        NfcUtils.mNfcAdapter.enableForegroundDispatch(
            this,
            NfcUtils.mPendingIntent,
            NfcUtils.mIntentFilter,
            NfcUtils.mTechList
        );
    }

    override fun onPause() {
        super.onPause()
        NfcUtils.mNfcAdapter.disableForegroundDispatch(this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        //当该Activity接收到NFC标签时，运行该方法
        //调用工具方法，读取NFC数据
        val str: String = NfcUtils.readNFCFromTag(intent)

        if (str.isNullOrEmpty()) {
//            val tag: Tag? = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG)
//
//            val tagId: ByteArray = (tag?.id ?: "") as ByteArray
//
//            var str: String = NfcUtils.ByteArrayToHexString(tagId)

//            str = flipHexStr(str)
//
//            val cardNo = str.toLong(16)
//
//            val ignoreOperationId: String = m_operationid
//
//            if (m_isOnline) {
//// if select all , pass in the operation id encoded in ticket
//
//// there should NOT be many operations, take the only one
//                CardValidationAsyncTask().execute(cardNo.toString())
//            }
            mDataBind.nfcText.text = "未能读取到数据"
        } else {
            mDataBind.nfcText.text = str
        }

        LogUtils.d("rogerzhang", "lllllllll-------------$str")
    }

}